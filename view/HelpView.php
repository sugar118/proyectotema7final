<?php

require_once 'lib/View.php';

class HelpView extends View{
    
    function __construct() {
        parent::__construct();
    }
    public function render($plantilla="help.tpl") {
        $this->smarty->display($plantilla);
    }
}
