<?php

require_once 'lib/View.php';

class LoginView extends View{

    function __construct() {
        parent::__construct();
    }

    public function render1($plantilla="index.tpl") {
        $this->smarty->assign("method",  $this->getMethod());
        $this->smarty->display($plantilla);
    }
    public function render2($plantilla="login.tpl") {
        $this->smarty->assign("method",  $this->getMethod());
        $this->smarty->display($plantilla);
    }
}

