<?php

class Acl{
    private $_acl = array(
        'index' => array(
            'index' => 3,
            'hello' => 3
        ),
        
        'user' => array(
            'index' => 1,
            'add' => 1,
            'edit' => 1,
            'delete' => 1,
            'update' => 1,
            'insert' => 1
        ),
        
        'study' => array(
            'index' => 1,
            'add' => 1,
            'edit' => 1,
            'delete' => 1,
            'update' => 1,
            'insert' => 1
        )
        
    );
    public function __construct(){
//        $this->_acl = cargarAclDelUsuario;
    }
    
    public function isAllowed($className, $method, $accessLevel){
        $className = strtolower($className);
        if (isset($this->_acl[$className][$method])){
            return  $accessLevel <= $this->_acl[$className][$method] ; 
        }
        else {return true;}
    }
}