<?php
require_once 'lib/Controller.php';
require_once 'model/LevelModel.php';

class Study extends Controller{
    private $_levelModel = LevelModel;

    function __construct()
    {
        parent::__construct('Study');
        $this->_levelModel = new LevelModel;
    }

   
    public function index()
    {
        //mostrar lista de todos los registros.
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }
    
    public function add($error="")
    {
        $levelRows = $this->_levelModel->getAll();
        $this->view->add($error, $levelRows);
    }
    
    public function insert()
    {
        $row = $_POST;  
        $row['password'] = md5($row['password']);
        $this->model->insert($row);    
        header('Location: ' . Config::URL . "/" . $_SESSION['lang'] . '/study/index');
    }
    public function delete($id)
    {
        $this->model->delete($id);    
        header('Location: ' . Config::URL . "/" .  $_SESSION['lang'] . '/study/index');
    }
    
    public function edit($id, $error="")
    {
        $row = $this->model->get($id);
        $levelRows = $this->_levelModel->getAll();
        $this->view->edit($row, $error, $levelRows);
    }

    public function update()
    {
        $row = $_POST; 
        $error = $this->_validate($row);
        if (count($error)){
            $this->edit($row['id'], $error);
        }
        else{
            $row['password'] = md5($row['password']);
            $this->model->update($row);    
            header('Location: ' . Config::URL . "/" .  $_SESSION['lang'] . '/study/index');
        }
    }
    
    private function _validate($row)
    {
        $error = array();
               
//        if (!preg_match("/^.{6,20}$/", $row['password'])){
//            $error['password'] = 'error_password';
//        }
        
        return $error;
    }
    
}