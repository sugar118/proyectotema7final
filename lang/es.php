<?php

$es = array(
    'app' => 'Aplicación de trabajo Tema 7 ',
    'operations' => 'Operaciones',
    'name' => 'Nombre',
    'password' => 'Contraseña',
    'user_list' => 'Lista de usuarios',
    'login' => 'Inicio Sesión',
    'edit' => 'Editar',
    'edit_user' => 'Editar Usuario',
    'delete' => 'Borrar',
    'new_user' => 'Nuevo usuario',
    'index' => 'Inicio',
    'help' => 'Ayuda',
    'user'=> 'Usuario',
    'error_password' => 'La contraseña debe tener entre 6 y 20 caracteres',
    'send'=> 'Enviar',
    'language' => 'Español',
    //
    'study'=> 'Estudios',
    'study_list' => 'Lista de estudios',
    'new_study' => 'Nuevo estudio',
    'innerCode' => 'Codigo Interno',
    'officialCode' => 'Codigo Oficial',
    'level' => 'Nivel',
    //controles select
    'select_one' => 'seleccionar uno  ------------',
);