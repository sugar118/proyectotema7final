<?php

$en = array(
    'app' => 'Work Application Subject 7 ',
    'operations' => 'Operations',
    'name' => 'Name',
    'password' => 'Password',
    'user_list' => 'User list',
    'login' => 'Login',
    'edit' => 'Edit',
    'edit_user' => 'Edit User',
    'delete' => 'Delete',
    'new_user' => 'New user',
    'index' => 'Index',
    'help' => 'Help',
    'user'=> 'User',
    'send'=> 'Send',
    'error_password' => 'The password must be between 6 and 20 characters',
    'language' => 'English',
    //
    'study'=> 'Estudies',
    'study_list' => 'Studies list',
    'new_study' => 'New study',
    'innerCode' => 'Inner code',
    'officialCode' => 'Official code',
    'level' => 'Level',
    //controles select
    'select_one' => 'select one   ------------',
);
