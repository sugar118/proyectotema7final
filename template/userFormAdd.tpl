{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2>{$language->translate('new_user')}</h2>
    
    <form action="{$url}/{$lang}/user/insert" method="post">
    
        <label>{$language->translate('name')}</label><input type="text" name="name"><br>
        <label>{$language->translate('password')}</label><input type="password" name="password"><br>
        <label>Role</label>
        
        <select name="idRole" >
            <option selected value="0">
            {foreach $roles as $role}
            <option value="{$role.id}">
                {$role.role} 
            </option>
            {/foreach}
        </select> 
        
        <br>
        <label></label><input type="submit" value="{$language->translate('send')}">
    </form>
    
</div>
{include file="template/footer.tpl" title="footer"}