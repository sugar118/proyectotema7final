{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2>{$language->translate('edit_user')}</h2>
    
    <form action="{$url}/{$lang}/user/update" method="post">
        <input type="hidden" name="id" value="{$row.id}">
        
        <label>{$language->translate('name')}</label><input type="text" name="name" value="{$row.usuario}"><br>
      
        <label>{$language->translate('password')}</label><input type="password" name="password"><br>
        <label>Role</label>
        
{*        <input type="text" name="idRole" value="{$row.idRole}"> {$row.role}*}
        
        <select name="idRole" >
            {foreach $roles as $role}
            <option {($role.id==$row.idRole)? 'selected' : ''} 
                value="{$role.id}">
                {$role.role}
            </option>
            {/foreach}
        </select> 
        
        <br>
        
        <!--<span class="error">{$language->translate($error.password)}</span> <br>-->
        <label></label><input type="submit" value="{$language->translate('send')}">
        
    </form>
    
</div>
{include file="template/footer.tpl" title="footer"}