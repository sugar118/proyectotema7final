{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('study_list')}</h2>
    
    <p><a href="{$url}/{$lang}/study/add">{$language->translate('new_study')}</a></p>
    <br><br><br>
    <table border="1">
        <tr>
            <th>Id</th>
            <th>{$language->translate('innerCode')}</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('level')}</th>
            <th>{$language->translate('officialCode')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
        
        {foreach $rows as $row}
            <tr>
                <td>{$row.id}</td>
                <td>{$row.codInterno}</td>
                <td>{$row.nombre}</td>
                <td>{$row.nivel}</td>
                <td>{$row.codOficial}</td>
                <td>
                    <a href="{$url}/{$lang}/study/edit/{$row.id}" >{$language->translate('edit')}</a>
                    <a href="{$url}/{$lang}/study/delete/{$row.id}" >{$language->translate('delete')}</a>                    
                </td>
            </tr>
        {/foreach}
    </table>

 </div>
{include file="template/footer.tpl" title="footer"}