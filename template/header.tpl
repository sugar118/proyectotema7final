<!DOCTYPE html>

<html>
    <head>
        <title>TODO: supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{$url}/public/css/default.css" />
        <link rel="stylesheet" href="{$url}/public/js/jquey.js" />
        
    </head>
    <body>
        <div id="header">
            
            <div id="title">
                {$language->translate('app')} - {$language->translate('language')}
            </div>
            
            <div  style="float: left">
                <!--Estamos en el {$title}-->
                <a href="{$url}/{$lang}/index" >{$language->translate('index')}</a> 
                <a href="{$url}/{$lang}/login" >{$language->translate('login')}</a>
                <a href="{$url}/{$lang}/help" >{$language->translate('help')}</a>
                <a href="{$url}/{$lang}/user" >{$language->translate('user')}</a>
                <a href="{$url}/{$lang}/study" >{$language->translate('study')}</a>
                
            </div>
            <div  style="float: right ; padding-left:4em">
                <a href="{$url}/es/index" >ES</a> 
                <a href="{$url}/en/index" >EN</a>
            </div>
        </div>
            
